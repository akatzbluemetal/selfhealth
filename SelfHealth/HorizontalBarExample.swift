//
//  ChartView.swift
//  SelfHealth
//
//  Created by Jim Wilcox on 2/17/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import UIKit
import CareKit

class HorizontalBarExample: UIView {
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  init() {
    super.init(frame: CGRect.zero)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  func initialize(value: [Decimal], percentComplete: [Decimal], metricTitle: [String], incentiveTitle: String) {
    
    
    let red = UIColor(red: 0xEF / 255.0, green: 0x44 / 255.0, blue: 0x5B / 255.0, alpha: 1.0)
    let purple = UIColor(red: 0xB2 / 255.0, green: 0x87 / 255.0, blue: 0xE6 / 255.0, alpha: 1.0)
    let yellow = UIColor(red: 0xDB / 255.0, green: 0xCE / 255.0, blue: 0x76 / 255.0, alpha: 1.0)
    
    var colors: [UIColor] = []
    
    colors.append(red)
    colors.append(purple)
    colors.append(yellow)
    
    for i in 0..<percentComplete.count {
      addHorizontalChart(value: value[i], percentComplete: percentComplete[i], color: colors[i], title: metricTitle[i], top: i * 50 + 30)
    }
    
    addLabel(text: incentiveTitle)
  }
  
  func addHorizontalChart(value: Decimal, percentComplete: Decimal, color: UIColor, title: String, top: Int){
    let goal = [100]
    let actual = [percentComplete]
    let gray = UIColor(red: 0xF0 / 255.0, green: 0xF0 / 255.0, blue: 0xF0 / 255.0, alpha: 1.0)
    let blue = UIColor(red: 0x3E / 255.0, green: 0xA1 / 255.0, blue: 0xEE / 255.0, alpha: 1.0)
    //let lightBlue = UIColor(red: 0x9C / 255.0, green: 0xCF / 255.0, blue: 0xF8 / 255.0, alpha: 1.0)
    
    let goalBarSeries = OCKBarSeries(title: "goal", values: goal as [NSNumber], valueLabels:
      ["goal"], tintColor: gray)
    let actualBarSeries = OCKBarSeries(title: "actual", values: actual as [NSNumber], valueLabels:
      [percentComplete.description + "%"], tintColor: color)
    
    let chart = OCKBarChart(title: "Incentive",
                            text: "Quest",
                            tintColor: blue,
                            axisTitles: [""],
                            axisSubtitles:[""],
                            dataSeries: [goalBarSeries, actualBarSeries],
                            minimumScaleRangeValue: 0,
                            maximumScaleRangeValue: 100)
    
    let labelHeight = CGFloat(16 + top)
    let chartView = chart.chartView()
    chartView.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(chartView)
    chartView.heightAnchor.constraint(equalToConstant: 38)
    chartView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 32).isActive = true
    chartView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
    chartView.topAnchor.constraint(equalTo: self.topAnchor, constant: labelHeight).isActive = true

    
    let label = UILabel()
    label.text = value.description
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.init(descriptor: UIFontDescriptor(), size: 32)
    label.textColor = color
    
    self.addSubview(label)
    label.heightAnchor.constraint(equalToConstant: 32).isActive = true
    label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
    label.rightAnchor.constraint(equalTo: self.rightAnchor).isActive=true
    let appleTop = CGFloat(top + 20)
    label.topAnchor.constraint(equalTo: self.topAnchor, constant: appleTop).isActive=true
    
    
    
    
    
    let titleLabel = UILabel()
    titleLabel.text = title
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.textColor = color
    
    self.addSubview(titleLabel)
    titleLabel.font = titleLabel.font.withSize(12)
    titleLabel.heightAnchor.constraint(equalToConstant: 32).isActive = true
    titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 52).isActive = true
    titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive=true
    let headerTop = CGFloat(top + 40)
    titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: headerTop).isActive=true
    
  }
  
  
  func addLabel(text: String) {
    let labelHeight = CGFloat(20)
    let label = UILabel()
    label.text = text
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.boldSystemFont(ofSize: 16)
    
    self.addSubview(label)
    label.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
    label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
    label.rightAnchor.constraint(equalTo: self.rightAnchor).isActive=true
    label.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive=true
    
  }
}
