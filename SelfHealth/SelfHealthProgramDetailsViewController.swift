//
//  SelfHealthProfileViewController.swift
//  SelfHealth
//
//  Created by Alexander Katz on 2/18/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import UIKit

class SelfHealthProgramDetailsViewController: UIViewController {
   override func viewDidLoad() {
    //let profileImage = UIColor(patternImage: )
    self.setNavBarInvisible(title: title)
    let img = UIImageView(image: #imageLiteral(resourceName: "ProgramProgress") )
    
    img.contentMode = .scaleAspectFit
    self.view.addAndConstrainChildViewToBounds(childView: img)
    
    
  }
  
}
