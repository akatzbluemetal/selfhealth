import Foundation

import UIKit

struct Constants {
  
  static let padding = CGFloat(8)
  
  static let programsCellHeight = CGFloat(132)
  static let trackingCellHeight = CGFloat(200)
  
  static let programsTopColor = UIColor(red: 39/255.0, green: 192/255.0, blue: 185/255.0, alpha: 1)
  static let programsBottomColor = UIColor(red: 44/255.0, green: 164/255.0, blue: 126/255.0, alpha: 1)
  static let poopGreen = UIColor(red: 48/255.0, green: 92/255.0, blue: 78/255.0, alpha: 1)
  static let gray = UIColor(red: 211/255.0, green: 221/255.0, blue: 218/255.0, alpha: 1)
}
