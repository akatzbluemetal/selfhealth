//
//  ProgramViewController.swift
//  SelfHealth
//
//  Created by Alexander Katz on 2/17/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import UIKit
import HealthKit

class ProgramViewController: UIViewController {

  var programTitle: String?
  var titleLabel: UILabel?
  var segmentedControl: UISegmentedControl!
  let titleContainerHeight = CGFloat(88 + 8 + 8)
  var selectedSegment = 0
  
  required init(title: String? = nil) {
    programTitle = title
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = UIColor.white
    title = "Self Health Programs"
    
    navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    navigationController?.navigationBar.isTranslucent = true
    navigationController?.navigationBar.tintColor = UIColor.white
    navigationController?.navigationBar.barTintColor = Constants.programsTopColor
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    
    let stepCountQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "mm-dd-yyyy hh:mm:ss"
    let today = dateFormatter.date(from: "02-17-2017 00:00:00")
    let tomorrow = dateFormatter.date(from: "02-18-2017 00:00:00")
    
    if let today = today, let tomorrow = tomorrow, let stepCountQuantityType = stepCountQuantityType {
      HealthManager.default.getHealthKitSamples(startDate: today, endDate: tomorrow, sampleType: stepCountQuantityType) { samples in
        if let samples = samples {
          let quantitySamples = samples.map { $0 as? HKQuantitySample }
          let stepCount = HealthManager.default.sumQuantitySampleDoubleValues(unit: HKUnit.count(), samples: quantitySamples)
          print("Step count: " + stepCount.description)
        }
      }
    }
    
    let navHeight = navigationController!.navigationBar.frame.height
    let statusBarHeight = UIApplication.shared.statusBarFrame.height

    titleLabel = UILabel()
    titleLabel?.text = programTitle
    titleLabel?.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(titleLabel!)
    titleLabel?.topAnchor.constraint(equalTo: view.topAnchor, constant: navHeight + statusBarHeight + Constants.padding).isActive = true
    titleLabel?.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Constants.padding).isActive = true
    titleLabel?.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -Constants.padding).isActive = true
    titleLabel?.heightAnchor.constraint(equalToConstant: 88).isActive = true
    
    segmentedControl = UISegmentedControl(items: ["Program Details", "My Progress"])
    segmentedControl.selectedSegmentIndex = selectedSegment
    segmentedControl.tintColor = Constants.programsTopColor
    segmentedControl.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(segmentedControl)
    segmentedControl.heightAnchor.constraint(equalToConstant: 32).isActive = true
    segmentedControl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Constants.padding).isActive = true
    segmentedControl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -Constants.padding).isActive = true
    segmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: navHeight + statusBarHeight + Constants.padding + titleContainerHeight).isActive = true
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tabBarController?.tabBar.isHidden = true
  }
  
}
