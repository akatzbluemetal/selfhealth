import Foundation
import UIKit
import CoreData
import HealthKit


class DataManager {
  
  static let `default` = DataManager()
  
  public var leaderBoard = Array<LeaderboardRow>()
  lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "SelfHealth")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
      if let error = error {
        fatalError("Unresolved error \(error)")
      }
    })
    return container
  }()
  
  
  var user: User
  
  
  init() {
    
    user = User()
    
  }
  
  
  func saveContext() {
    let context = persistentContainer.viewContext
    if context.hasChanges {
      do {
        try context.save()
      } catch let error as NSError {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    }
  }
  
  func addIncentive(inc: Incentive) {
    self.user.addToIncentives(inc)
    saveContext()
  }
  
  func removeIncentive(inc: Incentive) {
    self.user.removeFromIncentives(inc)
    saveContext()
  }
  
  enum HealthMetric: String {
    case StepCount
    case FlightsClimbed
    case ExerciseMinutes
    case PoundsLost
    case SleepHours
    case StandHours
    
  }
  
  //  func getIncentiveProgress (inc: Incentive) {
  //    let metrics = inc.metrics?.allObjects as! [Metric]
  //    for metric in metrics {
  //      if let actualMetric = metric.actualmetric, let goalMetric = metric.goalmetric {
  //        let actualMetricValue = actualMetric.value
  //        let goalMetricValue = goalMetric.completionValue
  //      }
  //    }
  //  }
  
  func mockIncentives() -> [Incentive] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "mm-dd-yyyy hh:mm:ss"
    
    let currentUser = User(context: persistentContainer.viewContext)
    currentUser.firstName = "Ethan"
    currentUser.lastName = "DuBois"
    currentUser.weightLb = 250
    currentUser.cholesterolMmoL = 234
    
    // Yearly Step Challenge
    
    let yearBegin = dateFormatter.date(from: "01-01-2017 00:00:00")
    let yearEnd = dateFormatter.date(from: "12-31-2017 00:00:00")
    
    let yearlyStepChallenge = Incentive(context: persistentContainer.viewContext)
    if let startDate = yearBegin, let endDate = yearEnd {
      yearlyStepChallenge.name = "Yearly Step Challenge"
      yearlyStepChallenge.text = "Placeholder for yearly step challenge"
      yearlyStepChallenge.startDate = startDate as NSDate
      yearlyStepChallenge.endDate = endDate as NSDate
      yearlyStepChallenge.reward = "$200"
      yearlyStepChallenge.rewardDescription = "Visa giftcard"
      yearlyStepChallenge.isEnrolled = false
      
    }
    
    let metric = Metric(context: persistentContainer.viewContext)
    metric.healthMetric = "Total Steps"
    
    let actualStepMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = yearBegin, let endDate = yearEnd {
      actualStepMetric.startDate = startDate as NSDate
      actualStepMetric.endDate = endDate as NSDate
      actualStepMetric.value = 567832.0
    }
    actualStepMetric.healthMetric = "Total Steps"
    
    let goalStepMetric = GoalMetric(context: persistentContainer.viewContext)
    goalStepMetric.healthMetric = "Total Steps"
    goalStepMetric.completionValue = 3650000.0
    
    metric.actualmetric = actualStepMetric
    metric.goalmetric = goalStepMetric
    
    yearlyStepChallenge.addToMetrics(metric)
    
    currentUser.addToIncentives(yearlyStepChallenge)
    saveContext()
    
    // Bluemetal Stairmaster
    
    let blueMetalStairmaster = Incentive(context: persistentContainer.viewContext)
    if let startDate = yearBegin, let endDate = yearEnd {
      blueMetalStairmaster.name = "The BlueMetal Stairmaster"
      blueMetalStairmaster.text = "Placeholder for Bluemetal Stairmaster Challenge"
      blueMetalStairmaster.startDate = startDate as NSDate
      blueMetalStairmaster.endDate = endDate as NSDate
      blueMetalStairmaster.reward = "$250"
      blueMetalStairmaster.rewardDescription = "at Footlocker"
      blueMetalStairmaster.isEnrolled = false
      
    }
    
    let flightMetric = Metric(context: persistentContainer.viewContext)
    flightMetric.healthMetric = "Total Staircases Climbed"
    
    let actualFlightMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = yearBegin, let endDate = yearEnd {
      actualFlightMetric.startDate = startDate as NSDate
      actualFlightMetric.endDate = endDate as NSDate
      actualFlightMetric.value = 3241
    }
    actualFlightMetric.healthMetric = "Total Staircases Climbed"
    
    let goalFlightMetric = GoalMetric(context: persistentContainer.viewContext)
    goalFlightMetric.healthMetric = "Total Staircases Climbed"
    goalFlightMetric.completionValue = 5000.0
    
    flightMetric.actualmetric = actualFlightMetric
    flightMetric.goalmetric = goalFlightMetric
    
    blueMetalStairmaster.addToMetrics(flightMetric)
    
    currentUser.addToIncentives(blueMetalStairmaster)
    saveContext()
    
    // 6-Month Weight Loss Challenge
    var startDate = dateFormatter.date(from: "10-01-2016 00:00:00")
    let endDate: Date? = Date()
    
    let sixMonthWeightChallenge = Incentive(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      sixMonthWeightChallenge.name = "6-Month Weight Loss Challenge"
      sixMonthWeightChallenge.text = "Placeholder for weight loss challenge description"
      sixMonthWeightChallenge.startDate = startDate as NSDate
      sixMonthWeightChallenge.endDate = endDate as NSDate
      sixMonthWeightChallenge.reward = "$338"
      sixMonthWeightChallenge.rewardDescription = "off of 2018 premium"
      sixMonthWeightChallenge.isEnrolled = true
      
    }
    // pounds lost
    
    let weightMetric = Metric(context: persistentContainer.viewContext)
    weightMetric.healthMetric = "Total Pounds Lost"
    
    let actualWeightMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      actualWeightMetric.startDate = startDate as NSDate
      actualWeightMetric.endDate = endDate as NSDate
    }
    actualStepMetric.healthMetric = "Total Pounds Lost"
    
    let goalWeightMetric = GoalMetric(context: persistentContainer.viewContext)
    goalWeightMetric.healthMetric = "Total Pounds Lost"
    goalWeightMetric.completionValue = 15
    
    weightMetric.actualmetric = actualWeightMetric
    weightMetric.goalmetric = goalWeightMetric
    
    sixMonthWeightChallenge.addToMetrics(weightMetric)
    
        // exercise minutes
    
    let activeExerciseWeightMetric = Metric(context: persistentContainer.viewContext)
    activeExerciseWeightMetric.healthMetric = "Average Exercise Minutes"
    
    let actualExerciseWeightMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      actualExerciseWeightMetric.startDate = startDate as NSDate
      actualExerciseWeightMetric.endDate = endDate as NSDate
      actualExerciseWeightMetric.value = 22.0
    }
    actualExerciseWeightMetric.healthMetric = "Average Exercise Minutes"
    
    let goalExerciseWeightMetric = GoalMetric(context: persistentContainer.viewContext)
    goalExerciseWeightMetric.healthMetric = "Average Exercise Minutes"
    goalExerciseWeightMetric.completionValue = 30
    
    activeExerciseWeightMetric.actualmetric = actualExerciseWeightMetric
    activeExerciseWeightMetric.goalmetric = goalExerciseWeightMetric
    
    sixMonthWeightChallenge.addToMetrics(activeExerciseWeightMetric)
    
    // yearly doctor visits
    
    let doctorVisitWeightMetric = Metric(context: persistentContainer.viewContext)
    doctorVisitWeightMetric.healthMetric = "Yearly Doctor Visits"
    
    let actualdoctorVisitWeightMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      actualdoctorVisitWeightMetric.startDate = startDate as NSDate
      actualdoctorVisitWeightMetric.endDate = endDate as NSDate
      actualdoctorVisitWeightMetric.value = 1.0
    }
    actualdoctorVisitWeightMetric.healthMetric = "Yearly Doctor Visits"
    
    let goaldoctorVisitWeightMetric = GoalMetric(context: persistentContainer.viewContext)
    goaldoctorVisitWeightMetric.healthMetric = "Yearly Doctor Visits"
    goaldoctorVisitWeightMetric.completionValue = 3
    
    doctorVisitWeightMetric.actualmetric = actualdoctorVisitWeightMetric
    doctorVisitWeightMetric.goalmetric = goaldoctorVisitWeightMetric
    
    sixMonthWeightChallenge.addToMetrics(doctorVisitWeightMetric)
    
    // add to incentives
    
    currentUser.addToIncentives(sixMonthWeightChallenge)
    saveContext()
    
    // Active Month of Exercising
    
    startDate = dateFormatter.date(from: "02-01-2017 00:00:00")
    
    let activeMonthExercising = Incentive(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      activeMonthExercising.name = "Active Month of Exercising"
      activeMonthExercising.text = "Placeholder for active month of exercising description"
      activeMonthExercising.startDate = startDate as NSDate
      activeMonthExercising.endDate = endDate as NSDate
      activeMonthExercising.reward = "$127"
      activeMonthExercising.rewardDescription = "off of 2018 premium"
      
    }
    
    let activeExerciseMetric = Metric(context: persistentContainer.viewContext)
    activeExerciseMetric.healthMetric = "Exercise Minutes"
    
    let actualExerciseMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = yearBegin, let endDate = yearEnd {
      actualExerciseMetric.startDate = startDate as NSDate
      actualExerciseMetric.endDate = endDate as NSDate
    }
    actualExerciseMetric.healthMetric = "Exercise Minutes"
    
    let goalExerciseMetric = GoalMetric(context: persistentContainer.viewContext)
    goalExerciseMetric.healthMetric = "Exercise Minutes"
    goalExerciseMetric.completionValue = 1000
    
    weightMetric.actualmetric = actualExerciseMetric
    weightMetric.goalmetric = goalExerciseMetric
    
    activeMonthExercising.addToMetrics(activeExerciseMetric)
    
    currentUser.addToIncentives(activeMonthExercising)
    saveContext()
    
    // Weekly StairClimb Challenge
    
    startDate = dateFormatter.date(from: "02-12-2017 00:00:00")
    
    let weeklyStairclimbChallenge = Incentive(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      weeklyStairclimbChallenge.name = "Weekly StairClimb Challenge"
      weeklyStairclimbChallenge.text = "Placeholder for weekly stairclimb challenge description"
      weeklyStairclimbChallenge.startDate = startDate as NSDate
      weeklyStairclimbChallenge.endDate = endDate as NSDate
      weeklyStairclimbChallenge.reward = "$25"
      weeklyStairclimbChallenge.rewardDescription = "Visa giftcard"
      weeklyStairclimbChallenge.isEnrolled = false
      
    }
    
    let weeklyFlightsMetric = Metric(context: persistentContainer.viewContext)
    weeklyFlightsMetric.healthMetric = "Total Staircases Climbed"
    
    let actualWeeklyFlightMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = yearBegin, let endDate = yearEnd {
      actualWeeklyFlightMetric.startDate = startDate as NSDate
      actualWeeklyFlightMetric.endDate = endDate as NSDate
      actualWeeklyFlightMetric.value = 27.0
    }
    actualWeeklyFlightMetric.healthMetric = "Total Staircases Climbed"
    
    let goalWeeklyFlightMetric = GoalMetric(context: persistentContainer.viewContext)
    goalWeeklyFlightMetric.healthMetric = "Total Staircases Climbed"
    goalWeeklyFlightMetric.completionValue = 30
    
    weeklyFlightsMetric.actualmetric = actualWeeklyFlightMetric
    weeklyFlightsMetric.goalmetric = goalWeeklyFlightMetric
    
    weeklyStairclimbChallenge.addToMetrics(weeklyFlightsMetric)
    
    currentUser.addToIncentives(weeklyStairclimbChallenge)
    saveContext()
    
    // Single Workday Step Challenge
    
    startDate = Date()
    
    let singleWorkdayStepChallenge = Incentive(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      singleWorkdayStepChallenge.name = "Single Workday Step Challenge"
      singleWorkdayStepChallenge.text = "Placeholder for single workday step challenge description"
      singleWorkdayStepChallenge.startDate = startDate as NSDate
      singleWorkdayStepChallenge.endDate = endDate as NSDate
      singleWorkdayStepChallenge.reward = "$20"
      singleWorkdayStepChallenge.rewardDescription = "for office snacks"
    }
    
    let dailyStepMetric = Metric(context: persistentContainer.viewContext)
    dailyStepMetric.healthMetric = "Total Steps"
    
    let actualDailyStepMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = yearBegin, let endDate = yearEnd {
      actualDailyStepMetric.startDate = startDate as NSDate
      actualDailyStepMetric.endDate = endDate as NSDate
    }
    actualDailyStepMetric.healthMetric = "Total Steps"
    
    let goalDailyStepMetric = GoalMetric(context: persistentContainer.viewContext)
    goalDailyStepMetric.healthMetric = "Total Steps"
    goalDailyStepMetric.completionValue = 30
    
    dailyStepMetric.actualmetric = actualWeeklyFlightMetric
    dailyStepMetric.goalmetric = goalWeeklyFlightMetric
    
    singleWorkdayStepChallenge.addToMetrics(dailyStepMetric)
    
    currentUser.addToIncentives(singleWorkdayStepChallenge)
    saveContext()
    
    self.user = currentUser
    
    return currentUser.incentives?.allObjects as! [Incentive]
    
  }
  
  func getEnrolledIncentives () -> [Incentive] {
    let incentives = self.user.incentives?.allObjects as! [Incentive]
    
    var enrolledIncentives: [Incentive] = []
    
    for incentive in incentives {
      if incentive.isEnrolled == true {
        enrolledIncentives.append(incentive)
      }
    }
    
    return enrolledIncentives
  }
  
    
  func testCode() {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "mm-dd-yyyy hh:mm:ss"
    
    let currentUser = User(context: persistentContainer.viewContext)
    currentUser.firstName = "Ethan"
    currentUser.lastName = "DuBois"
    currentUser.weightLb = 250
    currentUser.cholesterolMmoL = 234
    
    let startDate = dateFormatter.date(from: "02-12-2017 00:00:00")
    let endDate = dateFormatter.date(from: "02-18-2017 00:00:00")
    let weeklyStepChallenge = Incentive(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      weeklyStepChallenge.name = "Weekly Step Challenge"
      weeklyStepChallenge.startDate = startDate as NSDate
      weeklyStepChallenge.endDate = endDate as NSDate
      
    }
    currentUser.addToIncentives(weeklyStepChallenge)
    
    let actualStepMetric = ActualMetric(context: persistentContainer.viewContext)
    if let startDate = startDate, let endDate = endDate {
      actualStepMetric.startDate = startDate as NSDate
      actualStepMetric.endDate = endDate as NSDate
    }
    
    let goalStepMetric = GoalMetric(context: persistentContainer.viewContext)
    goalStepMetric.healthMetric = "StepCount"
    goalStepMetric.completionValue = 50000.0
    
    
  }
  
}









