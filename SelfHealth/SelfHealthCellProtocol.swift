import UIKit

protocol SelfHealthCell: class {
  
  static var identifier: String { get }
  static var nib: UINib? { get }
  
  var initialized: Bool { get set }
  
  func update(title: String)
  
  func initialize()
}

extension SelfHealthCell {
  static var nib: UINib? { return nil }
}
