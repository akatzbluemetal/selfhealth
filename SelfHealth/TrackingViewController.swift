import UIKit

class TrackingViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
  
  var collectionView: UICollectionView!
  
  var incentives: [Incentive] = []
  
  var isInitialized: Bool = false;
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.setVerticalGradient(colors: [Constants.programsTopColor, Constants.programsBottomColor])
    title = "Tracking"
    self.setNavBarInvisible(title: title)
    
    
    collectionView = UICollectionView(
      frame: view.bounds,
      collectionViewLayout: ProgramsCollectionViewLayout(
        size: view.bounds.size,
        cellSize: CGSize(
          width: view.bounds.width - (Constants.padding * 2),
          height: Constants.trackingCellHeight
        ),
        cellCount: 1
      )
    )
    
    collectionView.backgroundColor = nil
    collectionView.backgroundView = nil
    collectionView.register(TrackingCollectionViewCell.self, forCellWithReuseIdentifier: TrackingCollectionViewCell.identifier)
    collectionView.dataSource = self
    collectionView.delegate = self
    view.addAndConstrainChildViewToBounds(childView: collectionView)
    //collectionView.addAlphaGradientToSuperview()

  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    incentives = DataManager.default.getEnrolledIncentives()
    
    if isInitialized == false {
      isInitialized = true;
      self.collectionView.delegate = self
      self.collectionView.dataSource = self
      self.collectionView.reloadData()

    }
    
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return incentives.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TrackingCollectionViewCell.identifier, for: indexPath) as! TrackingCollectionViewCell
    let incentive = incentives[indexPath.item]
    let incentiveTitle = incentive.name!
    let metrics = incentive.metrics?.allObjects as! [Metric]
    var value: [Decimal] = []
    var percentComplete: [Decimal] = []
    var metricTitle: [String] = []
    for metric in metrics {
      if let actualMetric = metric.actualmetric, let goalMetric = metric.goalmetric {
        if let title = metric.healthMetric, let actualValue = actualMetric.value {
          value.append(actualValue as Decimal)
          percentComplete.append((actualValue as Decimal) / (goalMetric.completionValue as! Decimal))
          metricTitle.append(title)
        }
      }
    }
    cell.update(value: value, percentComplete: percentComplete, metricTitle: metricTitle, incentiveTitle: incentiveTitle)
    return cell
  }
}
