//
//  LeaderboardRow.swift
//  SelfHealth
//
//  Created by Apple on 2/18/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import Foundation

class LeaderboardRow : NSObject{
  
  var Metric:String
  var Unit: String
  var Period: String
  var username: String
  var UserDisplayName: String
  var NumberOfPrograms:Int
  
  init(_ JSON:NSDictionary) {
    self.Metric = JSON.value(forKeyPath: "Metric") as! String
    self.NumberOfPrograms = JSON.value(forKeyPath: "NumberOfPrograms") as! Int
    self.Period = JSON.value(forKeyPath: "Period") as! String
    self.Unit = JSON.value(forKeyPath: "Unit") as! String
    self.UserDisplayName = JSON.value(forKeyPath: "UserDisplayName") as! String
    self.username = JSON.value(forKeyPath: "username") as! String
  }
  
}
