import UIKit

extension UIView {
  func setVerticalGradient(colors: [UIColor]) {
    let layer = CAGradientLayer()
    layer.frame = self.bounds
    layer.colors = colors.map { $0.cgColor }
    self.layer.insertSublayer(layer, at: 0)
  }
  
  func addAndConstrainChildViewToBounds(childView: UIView, insets: UIEdgeInsets? = nil) {
    childView.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(childView)
    childView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: insets?.left ?? 0).isActive = true
    childView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: insets?.right ?? 0).isActive = true
    childView.topAnchor.constraint(equalTo: self.topAnchor, constant: insets?.top ?? 0).isActive = true
    childView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: insets?.bottom ?? 0).isActive = true
  }
  
  func addAlphaGradientToSuperview() {
    let alphaGradient = CAGradientLayer()
    alphaGradient.frame = self.superview?.bounds ?? CGRect.zero
    alphaGradient.colors = [
      UIColor.clear.cgColor,
      UIColor.clear.cgColor,
      UIColor.black.cgColor,
      UIColor.black.cgColor,
      UIColor.clear.cgColor,
      UIColor.clear.cgColor,
      UIColor.clear.cgColor,
      UIColor.clear.cgColor
    ]
    alphaGradient.locations = [0.0, 0.06, 0.12, 1, 1, 1, 1, 1.0]
    self.superview?.layer.mask = alphaGradient
  }
}
