import UIKit

class TrackingCollectionViewCell: UICollectionViewCell {
  
  static var identifier: String { get { return "TrackingCollectionViewCell" } }
  
  var initialized: Bool = false
  
  private let chartView = HorizontalBarExample()
  
  func update(value: [Decimal], percentComplete: [Decimal], metricTitle: [String], incentiveTitle: String) {
    if !initialized { initialize(value: value, percentComplete: percentComplete, metricTitle: metricTitle, incentiveTitle: incentiveTitle) }

    
  }
  
  func initialize(value: [Decimal], percentComplete: [Decimal],  metricTitle: [String], incentiveTitle: String) {
    contentView.layer.cornerRadius = 12
    contentView.backgroundColor = UIColor.white
    contentView.clipsToBounds = true
    chartView.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(chartView)
    chartView.initialize(value: value, percentComplete: percentComplete, metricTitle: metricTitle, incentiveTitle: incentiveTitle)
    chartView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
    chartView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
    chartView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    chartView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
  }
  
  
}
