import UIKit
import LocalAuthentication
class SelfHealthTabBarController: UITabBarController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let context:LAContext = LAContext();
    var error:NSError?
    var success:Bool;
    let reason:String = "Please authenticate using TouchID.";
    
    //    if (context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error))
    //    {
    //        context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply: { (success, error) -> Void in
    //            if (success) {
    
    //
    //                self.view.setVerticalGradient(colors: [Constants.programsTopColor, Constants.programsBottomColor])
    //
    //                let programsViewController = ProgramsViewController()
    //                programsViewController.tabBarItem = UITabBarItem(title: "Programs", image: nil, tag: 0)
    //                programsViewController.tabBarItem.image = UIImage(named: "ProgramsIcon")
    //                programsViewController.tabBarItem.selectedImage = UIImage(named: "ProgramsIcon-Active")
    //
    //                let trackingViewController = TrackingViewController()
    //                trackingViewController.tabBarItem = UITabBarItem(title: "Tracking", image: nil, tag: 1)
    //                trackingViewController.tabBarItem?.image = UIImage(named: "TrackingIcon")
    //                trackingViewController.tabBarItem?.selectedImage = UIImage(named: "TrackingIcon-Active")
    //
    //                let leadersViewController = LeadersViewController()
    //                leadersViewController.tabBarItem = UITabBarItem(title: "Leaders", image: nil, tag: 2)
    //                leadersViewController.tabBarItem?.image = UIImage(named: "LeadersIcon")
    //                leadersViewController.tabBarItem?.selectedImage = UIImage(named: "LeadersIcon-Active")
    //
    //                let profileViewController = SelfHealthProfileViewController()
    //                profileViewController.tabBarItem = UITabBarItem(title: "Profile", image: nil, tag: 3)
    //                profileViewController.tabBarItem?.image = UIImage(named: "ProfileIcon")
    //                profileViewController.tabBarItem?.selectedImage = UIImage(named: "ProfileIcon-Active")
    //
    //                self.viewControllers = [
    //                    trackingViewController,
    //                    programsViewController,
    //                    leadersViewController,
    //                    profileViewController
    //                    ].map { UINavigationController(rootViewController: $0) }
    //
    //                self.selectedViewController = self.viewControllers?[1]            }
    //            else
    //            {
    //
    //            }
    //        });
    
    self.tabBar.barTintColor = Constants.poopGreen
    self.tabBar.tintColor = Constants.programsBottomColor
    view.setVerticalGradient(colors: [Constants.programsTopColor, Constants.programsBottomColor])
    
    let programsViewController = ProgramsViewController()
    programsViewController.tabBarItem = UITabBarItem(title: "Programs", image: nil, tag: 0)
    programsViewController.tabBarItem.image = UIImage(named: "ProgramsIcon")
    programsViewController.tabBarItem.selectedImage = UIImage(named: "ProgramsIcon-Active")
    
    let trackingViewController = TrackingViewController()
    trackingViewController.tabBarItem = UITabBarItem(title: "Tracking", image: nil, tag: 1)
    trackingViewController.tabBarItem?.image = UIImage(named: "TrackingIcon")
    trackingViewController.tabBarItem?.selectedImage = UIImage(named: "TrackingIcon-Active")
    
    let leadersViewController = LeadersViewController()
    leadersViewController.tabBarItem = UITabBarItem(title: "Leaders", image: nil, tag: 2)
    leadersViewController.tabBarItem?.image = UIImage(named: "LeadersIcon")
    leadersViewController.tabBarItem?.selectedImage = UIImage(named: "LeadersIcon-Active")
    
    let profileViewController = SelfHealthProfileViewController()
    profileViewController.tabBarItem = UITabBarItem(title: "Profile", image: nil, tag: 3)
    profileViewController.tabBarItem?.image = UIImage(named: "ProfileIcon")
    profileViewController.tabBarItem?.selectedImage = UIImage(named: "ProfileIcon-Active")
    
    viewControllers = [
      trackingViewController,
      programsViewController,
      leadersViewController,
      profileViewController
      ].map { UINavigationController(rootViewController: $0) }
    
    selectedViewController = viewControllers?[1]
    
  }
}
