//
//  SelfHealthProfileViewController.swift
//  SelfHealth
//
//  Created by Alexander Katz on 2/18/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import UIKit

class SelfHealthProfileViewController: UIViewController {
   override func viewDidLoad() {
    //let profileImage = UIColor(patternImage: #imageLiteral(resourceName: "ProfileRender"))
    self.setNavBarInvisible(title: title)
    let img = UIImageView(image: #imageLiteral(resourceName: "ProfileRender"))
    
    img.contentMode = .scaleAspectFit
    self.view.addAndConstrainChildViewToBounds(childView: img)
    
    
  }
  
}
