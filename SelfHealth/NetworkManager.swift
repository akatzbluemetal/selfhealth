import Foundation
import Alamofire
import SwiftyJSON

class NetworkManager {
  static let `default` = NetworkManager()
  
  
  
  func getLeaderboard(metric: String, period: String, completion: @escaping (([LeaderboardRow]) -> Void)) {
      Alamofire.request("https://selfhealth.azurewebsites.net/api/leaderboard/\(metric)/\(period)").responseJSON { response in
      print(response.request as Any)  // original URL request
      print(response.response as Any) // HTTP URL response
      print(response.data as Any)     // server data
      print(response.result as Any)   // result of response serialization
      
        switch response.result{
        case .success(let JSON):
          DataManager.default.leaderBoard.removeAll()
          for obj in (JSON as! NSArray){
            print(obj)
            let dic = obj as! NSDictionary
            let lbr = LeaderboardRow(dic)
            DataManager.default.leaderBoard.append(lbr)
          }
          completion(DataManager.default.leaderBoard)
        case .failure(let error):
          print("Request failed with error: \(error)")
        
        }
        /*
        //to get status code
        if let status = response.response?.statusCode {
          switch(status){
          case 201:
            print("example success")
          default:
            print("error with response status: \(status)")
          }
        }
        
        //get json
        let json = JSON(data: response.data!)
        //let json = JSON(data: response.data!),
        //if let userDisplayName = json[].array {
          //Now you got your value
          //return incentiveName
        //}
    */
    
  
  }

}

}
