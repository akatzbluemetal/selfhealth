//
//  LeaderCollectionViewCell.swift
//  SelfHealth
//
//  Created by Alexander Katz on 2/18/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import UIKit

class LeaderCollectionViewCell: UICollectionViewCell {
  
  static let identifier = "LeaderCollectionViewCell"
  static let nib = UINib(nibName: LeaderCollectionViewCell.identifier, bundle: nil)
  
  @IBOutlet weak var rankLabel: UILabel!
  @IBOutlet weak var profilePictureImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var programsCountLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func update(rank: String, name: String, programsCount: Int, value: Int) {
    rankLabel.text = rank
    nameLabel.text = name
    programsCountLabel.text = "\(programsCount) Program\(programsCount != 1 ? "s" : "")"
    valueLabel.text = "\(value)"
    profilePictureImageView.image = UIImage(named: "Profiles-JoseRivera")
  }
  
}
