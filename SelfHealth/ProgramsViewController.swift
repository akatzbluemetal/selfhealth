import UIKit

class ProgramsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
  
  var collectionView: UICollectionView!
  
  let incentives: [Incentive] = DataManager.default.mockIncentives()
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.setVerticalGradient(colors: [Constants.programsTopColor, Constants.programsBottomColor])
    title = "Programs"
    
    collectionView = UICollectionView(
      frame: view.bounds,
      collectionViewLayout: ProgramsCollectionViewLayout(
        size: view.bounds.size,
        cellSize: CGSize(
          width: view.bounds.width - (Constants.padding * 2),
          height: Constants.programsCellHeight
        ),
        cellCount: incentives.count
      )
    )
    
    collectionView.backgroundColor = UIColor.clear
    collectionView.backgroundView = nil
    collectionView.register(ProgramsCollectionViewCell.nib!, forCellWithReuseIdentifier: ProgramsCollectionViewCell.identifier)
    collectionView.dataSource = self
    collectionView.delegate = self
    
    view.addAndConstrainChildViewToBounds(childView: collectionView)
    //collectionView.addAlphaGradientToSuperview()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setNavBarInvisible(title: title)
    tabBarController?.tabBar.isHidden = false
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return incentives.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProgramsCollectionViewCell.identifier, for: indexPath) as! ProgramsCollectionViewCell
    if let name = incentives[indexPath.item].name, let reward = incentives[indexPath.item].reward, let rewardDescription = incentives[indexPath.item].rewardDescription {
      cell.update(title: name, reward: reward, rewardDescription: rewardDescription)
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if let title = incentives[indexPath.item].name {
      let title = title
      let programViewController = ProgramViewController(title: title)
      navigationController?.pushViewController(programViewController, animated: true)
    }
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //navigationItem.titleView?.layer.shadowOpacity = Float(scrollView.contentOffset.y + 44) * Float(0.006)
  }
  
}
