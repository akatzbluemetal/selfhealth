import UIKit

extension UIViewController {
  func setNavBarInvisible(title: String? = nil) {
    
    navigationController?.navigationBar.tintColor = UIColor.white
    navigationController?.navigationBar.barTintColor = Constants.programsTopColor
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 50))
    label.text = title
    label.textAlignment = .center
    label.font = UIFont.boldSystemFont(ofSize: 18)
    label.textColor = UIColor.white
    label.layer.shadowColor = UIColor.black.cgColor
    label.layer.shadowRadius = 12
    label.layer.shadowOpacity = 0
    label.layer.shadowOffset = CGSize(width: 0, height: 0)
    self.navigationItem.titleView = label
  }
}
