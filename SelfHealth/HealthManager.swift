//
//  HealthManager.swift
//  SelfHealth
//
//  Created by Ethan DuBois on 2/17/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import Foundation
import HealthKit

class HealthManager {
  
  static let `default` = HealthManager()
  
  let healthKitStore: HKHealthStore = HKHealthStore()
  
  private func testCode() {
    // Example health kit code
    
    let healthManager = HealthManager.default
    
    healthManager.authorizeHealthKit { (success, error) -> Void in }
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "mm-dd-yyyy hh:mm:ss"
    let startDate = dateFormatter.date(from: "02-10-2017 00:00:00")
    let endDate = dateFormatter.date(from: "02-13-2017 00:00:00")
    
    let stepCountQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
    
    if let startDate = startDate, let endDate = endDate, let stepCountQuantityType = stepCountQuantityType {
      healthManager.getHealthKitSamples(startDate: startDate, endDate: endDate, sampleType: stepCountQuantityType) { samples in
        if let samples = samples {
          let quantitySamples = samples.map { $0 as? HKQuantitySample }
          let stepCount = healthManager.sumQuantitySampleDoubleValues(unit: HKUnit.count(), samples: quantitySamples)
          print("Step count: " + stepCount.description)
        }
      }
    }
    
    let flightsClimbedQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.flightsClimbed)
    
    if let startDate = startDate, let endDate = endDate, let flightsClimbedQuantityType = flightsClimbedQuantityType {
      healthManager.getHealthKitSamples(startDate: startDate, endDate: endDate, sampleType: flightsClimbedQuantityType) { samples in
        if let samples = samples {
          let quantitySamples = samples.map { $0 as? HKQuantitySample }
          let flightsClimbed = healthManager.sumQuantitySampleDoubleValues(unit: HKUnit.count(), samples: quantitySamples)
          print("Flights climbed: " + flightsClimbed.description)
        }
      }
    }
    
    let activeEnergyQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
    if let startDate = startDate, let endDate = endDate, let activeEnergyQuantityType = activeEnergyQuantityType {
      healthManager.getHealthKitSamples(startDate: startDate, endDate: endDate, sampleType: activeEnergyQuantityType) { samples in
        if let samples = samples {
          let quantitySamples = samples.map { $0 as? HKQuantitySample }
          let activeEnergy = healthManager.sumQuantitySampleDoubleValues(unit: HKUnit.kilocalorie(), samples: quantitySamples)
          print("Active energy KCal: " + activeEnergy.description)
        }
      }
    }
    
    let exerciseTimeQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.appleExerciseTime)
    if let startDate = startDate, let endDate = endDate, let exerciseTimeQuantityType = exerciseTimeQuantityType {
      healthManager.getHealthKitSamples(startDate: startDate, endDate: endDate, sampleType: exerciseTimeQuantityType) { samples in
        if let samples = samples {
          let quantitySamples = samples.map { $0 as? HKQuantitySample }
          let exerciseTime = healthManager.sumQuantitySampleDoubleValues(unit: HKUnit.minute(), samples: quantitySamples)
          print("Exercise Time Minutes: " + exerciseTime.description)
        }
      }
    }
    
    let heightQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)
    if let heightQuantityType = heightQuantityType {
      healthManager.getMostRecentHealthKitSample(sampleType: heightQuantityType) { sample in
        if let sample = sample as? HKQuantitySample {
          print("Most recent height: \(sample.quantity.doubleValue(for: HKUnit.inch()))")
        }
      }
    }
    
    let bodyMassQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)
    if let bodyMassQuantityType = bodyMassQuantityType {
      healthManager.getMostRecentHealthKitSample(sampleType: bodyMassQuantityType) { sample in
        if let sample = sample as? HKQuantitySample {
          print("Most recent weight: \(sample.quantity.doubleValue(for: HKUnit.pound()))")
        }
      }
    }
    
    let standingHoursCategoryType = HKCategoryType.categoryType(forIdentifier: HKCategoryTypeIdentifier.appleStandHour)
    if let startDate = startDate, let endDate = endDate, let standingHoursCategoryType = standingHoursCategoryType {
      healthManager.getHealthKitSamples(startDate: startDate, endDate: endDate, sampleType: standingHoursCategoryType) { samples in
        if let samples = samples {
          let categorySamples = samples.map { $0 as? HKCategorySample }
          let standingHours = healthManager.sumCategorySampleIntValues(samples: categorySamples)
          print("Standing hours: " + standingHours.description)
        }
      }
    }
    
    let sleepAnalysisCategoryType = HKCategoryType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)
    if let startDate = startDate, let endDate = endDate, let sleepAnalysisCategoryType = sleepAnalysisCategoryType {
      healthManager.getHealthKitSamples(startDate: startDate, endDate: endDate, sampleType: sleepAnalysisCategoryType) { samples in
        if let samples = samples {
          let categorySamples = samples.map { $0 as? HKCategorySample }
          let sleepAnalysis = healthManager.sumCategorySampleIntValues(samples: categorySamples)
          print("Sleep Analysis: " + sleepAnalysis.description)
        }
      }
    }
    
    print("Biological Sex: " + healthManager.getUserSex())
  }
  
  
  func authorizeHealthKit(completion: @escaping ((Bool, Error?) -> Void)) {
    
    
    var healthKitTypesToRead = Set<HKObjectType>()
    
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.appleExerciseTime)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryCholesterol)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.flightsClimbed)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic)!)
    healthKitTypesToRead.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic)!)
    healthKitTypesToRead.insert(HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!)
    healthKitTypesToRead.insert(HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.appleStandHour)!)
    healthKitTypesToRead.insert(HKObjectType.workoutType())
    healthKitTypesToRead.insert(HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.biologicalSex)!)
    healthKitTypesToRead.insert(HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.bloodType)!)
    healthKitTypesToRead.insert(HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.dateOfBirth)!)
    
    
    if !HKHealthStore.isHealthDataAvailable() {
      let error = NSError(domain: "com.bluemetal.SelfHealthApp", code: 2, userInfo: [NSLocalizedDescriptionKey: "HealthKit is not available in this Device"])
      completion(false, error)
      return
    }
    
    healthKitStore.requestAuthorization(toShare: nil, read: healthKitTypesToRead) { (success, error) -> Void in
      
      completion(success, error)
    }
  }
  
  func getHealthKitSamples(startDate: Date, endDate: Date, sampleType: HKSampleType, completion: @escaping (([HKSample]?) -> Void)) {
    
    let predicate = HKQuery.predicateForSamples(withStart: startDate as Date, end: endDate as Date, options: [])
    
    let stepsSampleQuery = HKSampleQuery(sampleType: sampleType,
                                         predicate: predicate,
                                         limit: Int(HKObjectQueryNoLimit),
                                         sortDescriptors: nil) { (query, results, error) in
                                          if error == nil {
                                            completion(results)
                                          } else {
                                            completion(nil)
                                          }
    }
    
    healthKitStore.execute(stepsSampleQuery)
    
    
  }
  
  func getMostRecentHealthKitSample(sampleType: HKSampleType, completion: @escaping ((HKSample?) -> Void)) {
    
    let predicate = HKQuery.predicateForSamples(withStart: NSDate.distantPast as Date, end: NSDate() as Date, options: [])
    
    let stepsSampleQuery = HKSampleQuery(sampleType: sampleType,
                                         predicate: predicate,
                                         limit: Int(HKObjectQueryNoLimit),
                                         sortDescriptors: nil) { (query, results, error) in
                                          if error == nil {
                                            completion(results?.first)
                                          } else {
                                            completion(nil)
                                          }
    }
    
    healthKitStore.execute(stepsSampleQuery)
    
    
  }
  
  
  func sumQuantitySampleDoubleValues(unit: HKUnit, samples: [HKQuantitySample?]) -> Double {
    
    var sum: Double = 0
    
    for sample in samples {
      sum += (sample?.quantity.doubleValue(for: unit)) ?? 0
    }
    
    return sum
    
  }
  
  
  func sumCategorySampleIntValues(samples: [HKCategorySample?]) -> Int {
    
    var sum: Int = 0
    
    for sample in samples {
      sum += (sample?.value) ?? 0
    }
    
    return sum
    
  }
  
  func getUserSex() -> String {
    
    do {
      let userSex = try healthKitStore.biologicalSex().biologicalSex
      var userSexString: String = "Unknown"
      switch userSex.rawValue {
        
      case 1:
        userSexString = "Female"
      case 2:
        userSexString = "Male"
      case 3:
        userSexString = "Other"
      default:
        userSexString = "Unknown"
      }
      return userSexString
      
    } catch {
      print(error)
      return "Unknown"
    }
  }
}
