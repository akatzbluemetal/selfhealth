//
//  LeadersViewController.swift
//  SelfHealth
//
//  Created by Alexander Katz on 2/17/17.
//  Copyright © 2017 BlueMetal. All rights reserved.
//

import UIKit

class LeadersViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
  
  var pageControl: UIPageControl!
  var collectionView: UICollectionView!
  var segmentedControl: UISegmentedControl!
  
  let pageCount = 3
  var leaderboardRows: [LeaderboardRow]?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Leaders"
    
    setNavBarInvisible(title: title)
    
    view.setVerticalGradient(colors: [Constants.programsTopColor, Constants.programsBottomColor])
    
    let navHeight = navigationController!.navigationBar.frame.height
    let statusBarHeight = UIApplication.shared.statusBarFrame.height
    let tabBarHeight = tabBarController!.tabBar.frame.height
    let segmentedControlHeight = CGFloat(32 + Constants.padding + Constants.padding)
    
    let collectionViewLayout = UICollectionViewFlowLayout()
    collectionViewLayout.itemSize = CGSize(width: view.bounds.width, height: view.bounds.height - navHeight - statusBarHeight - tabBarHeight - segmentedControlHeight)
    collectionViewLayout.minimumLineSpacing = 0
    collectionViewLayout.minimumInteritemSpacing = 0
    collectionViewLayout.scrollDirection = .horizontal
    collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewLayout)
    collectionView.isPagingEnabled = true
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.showsHorizontalScrollIndicator = false
    collectionView.register(LeadersListCollectionViewCell.nib, forCellWithReuseIdentifier: LeadersListCollectionViewCell.identifier)
    collectionView.backgroundColor = UIColor.white
    
    let insets = UIEdgeInsets(top: navHeight + statusBarHeight + segmentedControlHeight, left: 0, bottom: 0, right: 0)
    view.addAndConstrainChildViewToBounds(childView: collectionView, insets: insets)
    
    pageControl = UIPageControl()
    pageControl.backgroundColor = UIColor.white
    pageControl.numberOfPages = pageCount
    pageControl.currentPage = 0
    pageControl.pageIndicatorTintColor = Constants.programsTopColor.withAlphaComponent(0.2)
    pageControl.currentPageIndicatorTintColor = Constants.programsTopColor
    pageControl.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(pageControl)
    pageControl.heightAnchor.constraint(equalToConstant: 22).isActive = true
    pageControl.topAnchor.constraint(equalTo: view.topAnchor, constant: navHeight + statusBarHeight + tabBarHeight + 10).isActive = true
    pageControl.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    pageControl.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    
    segmentedControl = UISegmentedControl(items: ["Individual Metrics", "Incentive Programs"])
    segmentedControl.selectedSegmentIndex = 0
    segmentedControl.tintColor = UIColor.white
    segmentedControl.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(segmentedControl)
    segmentedControl.heightAnchor.constraint(equalToConstant: 32).isActive = true
    segmentedControl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Constants.padding).isActive = true
    segmentedControl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -Constants.padding).isActive = true
    segmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: navHeight + statusBarHeight + Constants.padding).isActive = true
    
    DispatchQueue.global(qos: .userInitiated).async {
      NetworkManager.default.getLeaderboard(metric:"Steps",period:"Daily") { leaderboardRows in
        DispatchQueue.main.async {
          print(leaderboardRows)
        }
      }
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return pageCount
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LeadersListCollectionViewCell.identifier, for: indexPath) as! LeadersListCollectionViewCell
    if indexPath.item == 0 { cell.setTitle(title: "Step Count") }
    if indexPath.item == 1 { cell.setTitle(title: "Flights Climbed") }
    if indexPath.item == 2 { cell.setTitle(title: "Exercise Minutes") }
    return cell
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let pageWidth = collectionView.frame.size.width
    pageControl.currentPage = Int(self.collectionView.contentOffset.x / pageWidth)
  }
}
