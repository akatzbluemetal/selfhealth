import UIKit

class ProgramsCollectionViewCell: UICollectionViewCell {
  
  static var identifier: String { return "ProgramsCollectionViewCell" }
  static var nib: UINib? { return UINib(nibName: ProgramsCollectionViewCell.identifier, bundle: nil) }
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var rewardLabel: UILabel!
  @IBOutlet weak var rewardDescriptionLabel: UILabel!
  
  var initialized: Bool = false
  
  func update(title: String, reward: String, rewardDescription: String) {
    if !initialized { initialize() }
    titleLabel.text = title
    rewardLabel.text = reward
    rewardDescriptionLabel.text = rewardDescription
  }
  
  func initialize() {
    contentView.layer.cornerRadius = 12
    contentView.backgroundColor = UIColor.white
    rewardLabel.textColor = Constants.programsTopColor
    rewardDescriptionLabel.textColor = Constants.gray
    rewardDescriptionLabel.text = "some long stuffs to see stuff and stuff and stuff and stuff and some more stuff!"
    rewardDescriptionLabel.numberOfLines = 0
    rewardDescriptionLabel.sizeToFit()
  }
}
