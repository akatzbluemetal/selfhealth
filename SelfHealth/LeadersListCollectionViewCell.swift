import UIKit

class LeadersListCollectionViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
  
  static let identifier = "LeadersListCollectionViewCell"
  static let nib = UINib(nibName: LeadersListCollectionViewCell.identifier, bundle: nil)
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.register(LeaderCollectionViewCell.nib, forCellWithReuseIdentifier: LeaderCollectionViewCell.identifier)
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .vertical
    layout.minimumLineSpacing = 0
    layout.minimumInteritemSpacing = 0
    layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 88)
    collectionView.collectionViewLayout = layout
    titleLabel.textColor = Constants.programsTopColor
    titleLabel.backgroundColor = UIColor.white
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return 10
  }
  
  func setTitle(title: String) {
    titleLabel.text = title
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LeaderCollectionViewCell.identifier, for: indexPath) as! LeaderCollectionViewCell
    cell.update(rank: "1", name: "Jose", programsCount: 4, value: 4323)
    return cell
  }
  
}
