import UIKit

class ProgramsCollectionViewLayout: UICollectionViewLayout {
  
  private var size: CGSize = CGSize.zero
  private var cellSize: CGSize = CGSize.zero
  private var cellCount: Int = 0
  private var layoutAttributes: [UICollectionViewLayoutAttributes] = []
  
  required init(size: CGSize, cellSize: CGSize, cellCount: Int) {
    super.init()
    self.size = size
    self.cellSize = cellSize
    self.cellCount = cellCount
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    return self.layoutAttributes
  }
  
  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    return self.layoutAttributes.filter { layoutAttributes in
      (layoutAttributes.indexPath as NSIndexPath).item == (indexPath as NSIndexPath).item
      }.first
  }
  
  override var collectionViewContentSize : CGSize {
    return CGSize(
      width: size.width,
      height: Constants.padding + ((cellSize.height + Constants.padding) * CGFloat(cellCount))
    )
  }
  
  override func prepare() {
    for i in 0..<cellCount {
      let indexPath = IndexPath(item: i, section: 0)
      let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
      let x = Constants.padding
      let y = Constants.padding + ((cellSize.height + Constants.padding) * CGFloat(i))
      
      attributes.frame = CGRect(
        origin: CGPoint(x: x, y: y),
        size: cellSize
      )
      
      layoutAttributes.append(attributes)
    }
  }
  
}
